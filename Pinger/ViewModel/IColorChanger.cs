﻿namespace Pinger.ViewModel
{
    public interface IColorChanger
    {
        void SetBrushFromRGB1(int[] RGBtab, int index);
        void SetBrushFromRGB2(int[] RGBtab, int index);
    }
}
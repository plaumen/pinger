﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using Pinger.ViewModel;

namespace Pinger.Model.PING
{
    public class PingSender:IPingSender
    {

        private PingViewModel viewModel;
        public long PingTime { get; set; }

        public PingSender(PingViewModel viewModel)
        {
            this.viewModel = viewModel;
        }

        public bool SendPing(string ipAddress)
        {
            try
            {
                var pingReply = new Ping().Send(IPAddress.Parse(ipAddress), viewModel.Timeout);
                PingTime = pingReply.RoundtripTime;
                return pingReply != null && pingReply.Status == IPStatus.Success;
                
            }
            catch(Exception)
            {
                return false;
            }
            
        }       
    }
}

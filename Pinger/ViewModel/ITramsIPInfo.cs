﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Pinger.ViewModel
{
    public interface ITramsIPInfo
    {
        void CheckTramIP(int index);
        void CheckAllTrams();
        void CheckAllTramsThread();
        void SetButtonsIPActive(int index);
        int[] SetProperColorForButton(ref List<bool> stateList, int index, int ip);
        void SetButtonsIPInactive();

    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pinger;
using Pinger.ViewModel;
using FluentAssertions;
using Xunit;
using System.Windows.Media;
using Pinger.Model.PING;
using NSubstitute;
using Pinger.Model.MVVM;

namespace Pinger.Tests
{

    public class PingViewModelTest
    {

        [Fact]
        public void AddTram_WhenMethodCall_ShouldAddNewTramToTramCollect()
        {
            //Arrange
            PingViewModel model = new PingViewModel();
            //Act
            model.AddTram();
            //Assert
            model.Trams.Should().HaveCount(1);
        }
         
        [Fact]
        public void AddTram_WhenMethodCallThreeTimes_ShouldAddNewThreeTramToCollection()
        {
            //Arrange
            PingViewModel model = new PingViewModel();
            //Act
            model.AddTram();
            model.AddTram();
            model.AddTram();
            //Assert
            model.Trams.Should().HaveCount(3);
        }

        [Fact]
        public void AddTram_WhenMethodCall99Times_ShouldAdd99TramToCollection()
        {
            //Arrange
            PingViewModel model = new PingViewModel();
            //Act
            for (int i = 0; i < 99; i++)
            {
                model.AddTram();
            }
            //Assert
            model.Trams.Should().HaveCount(99);
        }

        [Fact]
        public void AddTram_WhenIpIsWrong_ShouldTramCollectionBeNoChange()
        {
            //Arrange
            IMessageBoxWrapper messageBoxMock = Substitute.For<IMessageBoxWrapper>();
            IMessageBoxFactory factoryMock = Substitute.For<IMessageBoxFactory>();
            factoryMock.CreateMessageBox().Returns(messageBoxMock);
            PingViewModel model = new PingViewModel(factoryMock, "10.23");

            //Act
            model.AddTram();
            //Assert
            model.Trams.Should().BeEmpty();
        }
        [Fact]
        public void AddTram_WhenMethodCallAfterDeleteSomeElements_ShouldAddNewTramToTramCollect()
        {
            //Arrange
            PingViewModel model = new PingViewModel();
            for (int i = 0; i < 99; i++)
            {
                model.AddTram();
            }
            model.Trams.Remove(model.Trams.Last());
            //Act
            model.AddTram();
            //Assert
            model.Trams.Should().HaveCount(99);
        }

        [Fact]
        public void CheckTram_WhenSecondPingResponseIsCorrect_ShouldSetSecondButtonGreen()
        {
            //arrange
            IPingSender senderMock = Substitute.For<IPingSender>();
            IConfigurationLoader loaderMock = Substitute.For<IConfigurationLoader>();
            loaderMock.GetConfiguration("InactiveIPColorFirst").Returns("210, 250, 210");
            loaderMock.GetConfiguration("ActiveIPColorFirst").Returns("210, 250, 210");
            senderMock.SendPing("10.1.1.11").Returns(false);
            senderMock.SendPing("10.1.1.12").Returns(false);
            PingViewModel model = new PingViewModel( senderMock, loaderMock);
            //Act
            model.AddTram();
            model.TramsInfo.CheckTramIP(0);

            //Assert
            model.Trams[0].Ip1State.Should().Be(StatesIpTram.NOTRESPONDING);
            model.Trams[0].Ip2State.Should().Be(StatesIpTram.NOTRESPONDING);
        }

        [Fact]
        public void CheckTram_WhenFirstPingResponseIsCorrect_ShouldSetFirstButtonGreen()
        {
            //arrange
            IPingSender senderMock = Substitute.For<IPingSender>();
            IConfigurationLoader loaderMock = Substitute.For<IConfigurationLoader>();
            loaderMock.GetConfiguration("InactiveIPColorFirst").Returns("210, 250, 210");
            loaderMock.GetConfiguration("ActiveIPColorFirst").Returns("210, 250, 210");
            senderMock.SendPing("10.1.1.11").Returns(true);
            senderMock.SendPing("10.1.1.12").Returns(false);
            PingViewModel model = new PingViewModel() { Sender = senderMock,Loader = loaderMock };
            //Act
            model.AddTram();
            model.TramsInfo.CheckTramIP(0);

            //Assert
            model.Trams[0].Ip1State.Should().Be(StatesIpTram.RESPONDING);
            model.Trams[0].Ip2State.Should().Be(StatesIpTram.NOTRESPONDING);

        }

        [Fact]
        public void CheckTram_WhenBothPingResponsesAreCorrect_ShouldSetButtonsGreen()
        {
            //arrange
            IPingSender senderMock = Substitute.For<IPingSender>();
            IConfigurationLoader loaderMock = Substitute.For<IConfigurationLoader>();
            loaderMock.GetConfiguration("InactiveIPColorFirst").Returns("210, 250, 210");
            loaderMock.GetConfiguration("ActiveIPColorFirst").Returns("210, 250, 210");
            senderMock.SendPing("10.1.1.11").Returns(true);
            senderMock.SendPing("10.1.1.12").Returns(true);
            PingViewModel model = new PingViewModel() { Sender = senderMock, Loader = loaderMock };
            //Act
            model.AddTram();
            model.TramsInfo.CheckTramIP(0);
            //Assert
            model.Trams[0].Ip1State.Should().Be(StatesIpTram.RESPONDING);
            model.Trams[0].Ip2State.Should().Be(StatesIpTram.RESPONDING);
        }

        [Fact]
        public void CheckTram_WhenBothPingAreIncorrect_ShouldSetButtonsRed()
        {
            //arrange
            IPingSender senderMock = Substitute.For<IPingSender>();
            IConfigurationLoader loaderMock = Substitute.For<IConfigurationLoader>();
            loaderMock.GetConfiguration("InactiveIPColorFirst").Returns("210, 250, 210");
            loaderMock.GetConfiguration("ActiveIPColorFirst").Returns("210, 250, 210");
            senderMock.SendPing("10.1.1.11").Returns(false);
            senderMock.SendPing("10.1.1.12").Returns(false);
            PingViewModel model = new PingViewModel() { Sender = senderMock, Loader = loaderMock };
            //Act
            model.AddTram();
            model.TramsInfo.CheckTramIP(0);
            //Assert
            model.Trams[0].Ip1State.Should().Be(StatesIpTram.NOTRESPONDING);
            model.Trams[0].Ip2State.Should().Be(StatesIpTram.NOTRESPONDING);

        }
    }
}

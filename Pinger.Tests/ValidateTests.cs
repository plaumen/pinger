﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Pinger.Model.PING;
using Xunit;
using Pinger.ViewModel;


namespace Pinger.Tests
{
    public class ValidateTests
    {
        [Fact]
        public void CheckIp_WhenCallWithCorrectFormat_ShouldReturnTrue()
        {
            //Arrange
            var val = new IPAddressValidator();
            //Act
            var result = val.CheckIp("10.1.234.1");
            //Assert
            result.Should().Be(true);
        }


        [Fact]
        public void CheckIp_WhenCallWithTotalIncorrectFormat_ShouldReturnFalse()
        {
            //Arrange
            IPAddressValidator val = new IPAddressValidator();
            //Act
            bool result = val.CheckIp("ip");
            //Assert
            result.Should().Be(false);
            
        }

        [Fact]
        public void CheckIp_WhenCallWithAlmostCorrectFormat_ShouldReturnFalse()
        {
            //Arrange
            IPAddressValidator val = new IPAddressValidator();
            //Act
            bool result = val.CheckIp("10.256.1.252");
            //Assert
            result.Should().Be(false);
        }

        [Fact]
        public void CheckIp_WhenCallWithNumbersWithoutDots_ShouldReturnFalse()
        {
            //Arrange
            IPAddressValidator val = new IPAddressValidator();
            //Act
            bool result = val.CheckIp("102561252");
            //Assert
            result.Should().Be(false);
        }

        [Fact]
        public void CheckIp_WhenCallWithDots_ShouldReturnTrue()
        {
            //Arrange
            IPAddressValidator val = new IPAddressValidator();
            //Act
            bool result = val.CheckIp("0.0.0.0");
            //Assert
            result.Should().Be(true);
        }

        [Fact]
        public void CheckIp_WhenCallWithMaxCorrectNumbers_ShouldReturnTrue()
        {
            //Arrange
            IPAddressValidator val = new IPAddressValidator();
            //Act
            bool result = val.CheckIp("255.255.255.255");
            //Assert
            result.Should().Be(true);
        }
    }
}
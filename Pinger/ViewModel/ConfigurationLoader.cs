﻿using System.Configuration;

namespace Pinger.ViewModel
{
    public class ConfigurationLoader: IConfigurationLoader
    {
        public string GetConfiguration(string ConfigName)
        {
            return ConfigurationManager.AppSettings[ConfigName];
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media;
using Pinger.Model.PING;

namespace Pinger.ViewModel
{
    public class TramsIpInfo: ITramsIPInfo
    {
        private PingViewModel _viewModel;
        private IColorChanger _colorChanger;

        public TramsIpInfo(PingViewModel viewModel)
        {
            _viewModel = viewModel;
            _colorChanger = new ColorChanger(viewModel);
        }

        public void CheckTramIP(int index)
        {
            bool stateIp1 = _viewModel.Sender.SendPing(_viewModel.Trams[index].IP[0]);
            _viewModel.Trams[index].Ip1PingTime = _viewModel.Sender.PingTime;

            _viewModel.Trams[index].Ip1State = (stateIp1) ? StatesIpTram.RESPONDING : StatesIpTram.NOTRESPONDING;
            _viewModel.Trams[index].Ip1StateList.Add(stateIp1);

            bool stateIp2 = _viewModel.Sender.SendPing(_viewModel.Trams[index].IP[1]);
            _viewModel.Trams[index].Ip2PingTime = _viewModel.Sender.PingTime;

            _viewModel.Trams[index].Ip2State = (stateIp2) ? StatesIpTram.RESPONDING : StatesIpTram.NOTRESPONDING;
            _viewModel.Trams[index].Ip2StateList.Add(stateIp2);
            SetButtonsIPActive(index);
        }

        public void CheckAllTrams()
        {
            _viewModel.LastRefresh = DateTime.Now.ToString("h:mm:ss tt");
            Thread checkTramThread = new Thread(CheckAllTramsThread);
            checkTramThread.Start();
        }

        public void CheckAllTramsThread()
        {
            Parallel.For(0, _viewModel.Trams.Count, i =>
            {
                CheckTramIP(i);
            });
        }
         
        public void SetButtonsIPActive(int index)
        {

            _colorChanger.SetBrushFromRGB1(SetProperColorForButton(ref _viewModel.Trams[index].Ip1StateList, index, 1), index);
            _colorChanger.SetBrushFromRGB2(SetProperColorForButton(ref _viewModel.Trams[index].Ip2StateList, index, 2), index);
        }

        public int[] SetProperColorForButton(ref List<bool> stateList, int index, int ip)
        {
            if (stateList.Count > 1)
            {
                if ((stateList.Last() == true) && stateList[stateList.Count - 2] == false)
                {
                    stateList.RemoveRange(0, stateList.Count - 2);
                    if(ip==1) _viewModel.Trams[index].Ip1TimeOfLastChange = DateTime.Now.ToString("h:mm:ss tt");
                    else _viewModel.Trams[index].Ip2TimeOfLastChange = DateTime.Now.ToString("h:mm:ss tt");
                }
                if ((stateList.Last() == false) && stateList[stateList.Count - 2] == true)
                {
                    stateList.RemoveRange(0, stateList.Count - 2);
                    if (ip == 1) _viewModel.Trams[index].Ip1TimeOfLastChange = DateTime.Now.ToString("h:mm:ss tt");
                    else _viewModel.Trams[index].Ip2TimeOfLastChange = DateTime.Now.ToString("h:mm:ss tt");
                }
            }
            if (!stateList.Last())
            {
                string[] stringTab = _viewModel.Loader.GetConfiguration("InactiveIPColorFirst").Split(',');
                int[] tab = Array.ConvertAll(stringTab, int.Parse);

                int corrector = (stateList.Count < 8) ? (30*(stateList.Count-1)) : (180);
                tab[1] -= corrector;
                tab[2] -= corrector;
                return tab;
            }
            else
            {
                string[] stringTab = _viewModel.Loader.GetConfiguration("ActiveIPColorFirst").Split(',');
                int[] tab = Array.ConvertAll(stringTab, int.Parse);

                int corrector = (stateList.Count < 8) ? (30 * (stateList.Count - 1)) : (180);
                tab[0] -= corrector;
                tab[2] -= corrector;
                return tab;
            }
        }

        public void SetButtonsIPInactive()
        {
            for(int index =0;index<_viewModel.Trams.Count;index++)
            {
                _colorChanger.SetBrushFromRGB1(new[] {160, 160, 160}, index);
                _colorChanger.SetBrushFromRGB2(new[] {160, 160, 160}, index);
            }
        }
    }
}

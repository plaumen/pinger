﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Pinger.ViewModel;

namespace Pinger.Model.PING
{
    class TimerForCheckIpAddresses : ITimerForCheckIpAddresses
    {

        private int timeStart = 0;
        private PingViewModel viewModel;
        //private int interval; 

        public TimerForCheckIpAddresses(PingViewModel viewModel)
        {
            this.viewModel = viewModel;
            //interval = this.viewModel.RefreshPeriod;
        }

        public void StartTimer()
        {
            timeStart = Environment.TickCount;
            Timer oTimer = new Timer();
            oTimer.Elapsed +=new ElapsedEventHandler(OnTimeEvent);
            oTimer.Interval = viewModel.RefreshPeriod;
            oTimer.Enabled = true;
        }

        public void OnTimeEvent(object oSource, ElapsedEventArgs oElapsedEventArgs)
        {
            viewModel.TramsInfo.CheckAllTrams();
        }
    }
}

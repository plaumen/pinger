﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pinger.Model.PING;
using Pinger.Model.MVVM;
using System.Configuration;
using System.Messaging;

namespace Pinger.ViewModel
{
    public class RunService:IRunService
    {

        private IMessageBoxFactory _messageFactory;
        private IMessageBoxWrapper _wrapper;
        public RunService()
        {
            _messageFactory = new MessageBoxFactory();
            _wrapper = _messageFactory.CreateMessageBox();
        }

        public void RunPutty(Object tramFromUI)
        {
            SingleTram tram = (SingleTram)tramFromUI;
            if (tram.Ip1State == StatesIpTram.RESPONDING || tram.Ip2State == StatesIpTram.RESPONDING)
            {
                string ip = (tram.Ip1State == StatesIpTram.RESPONDING) ? tram.IP[0] : tram.IP[1];
                ProcessRunner.Run(ConfigurationManager.AppSettings["PuTTYPath"],
                    " -ssh " + ip + ConfigurationManager.AppSettings["PuTTYArgs"]);
            }
            else
            {
                _wrapper.ShowMessage("Żadne ip niedostępne !", "IpError", MessageType.Report);
            }
        }

        public void RunRDP(Object tramFromUI)
        {
            SingleTram tram = (SingleTram)tramFromUI;
            if (tram.Ip1State == StatesIpTram.RESPONDING || tram.Ip2State == StatesIpTram.RESPONDING)
            {
                string ip = (tram.Ip1State == StatesIpTram.RESPONDING) ? tram.IP[0] : tram.IP[1];
                ProcessRunner.Run(ConfigurationManager.AppSettings["RDPPath"],
                    " /v " + ip + ConfigurationManager.AppSettings["RDPArgs"]);
            }
            else
            {
                _wrapper.ShowMessage("Żadne ip niedostępne !", "IpError", MessageType.Report);
            }
        }

        public void RunWinSCP(Object tramFromUI)
        {
            SingleTram tram = (SingleTram)tramFromUI;
            if (tram.Ip1State == StatesIpTram.RESPONDING || tram.Ip2State == StatesIpTram.RESPONDING)
            {
                string ip = (tram.Ip1State == StatesIpTram.RESPONDING) ? tram.IP[0] : tram.IP[1];
                ProcessRunner.Run(ConfigurationManager.AppSettings["WinSCP"], ConfigurationManager.AppSettings["WinSCPAdditionalParams"] + ip + ":" + ConfigurationManager.AppSettings["WinSCPort"] + "/");
            }
            else
            {
                _wrapper.ShowMessage("Żadne ip niedostępne !", "IpError", MessageType.Report);
            }
        }
    }
}

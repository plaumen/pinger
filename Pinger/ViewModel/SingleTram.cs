﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pinger.Model.MVVM;
using Pinger.Model.PING;

namespace Pinger.ViewModel
{
    public class SingleTram:ObservableObject
    {
        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    RaisePropertyChanged("Name");
                }
            }
        }

        private string _ip1TimeOfLastChange;
        public string Ip1TimeOfLastChange
        {
            get { return _ip1TimeOfLastChange; }
            set
            {
                if (_ip1TimeOfLastChange != value)
                    _ip1TimeOfLastChange = value;
                RaisePropertyChanged("Ip1TimeOfLastChange");
            }
        }

        private string _ip2TimeOfLastChange;
        public string Ip2TimeOfLastChange
        {
            get { return _ip2TimeOfLastChange; }
            set
            {
                if (_ip2TimeOfLastChange != value)
                    _ip2TimeOfLastChange = value;
                RaisePropertyChanged("Ip2TimeOfLastChange");
            }
        }

        public List<string> IP = new List<string>();

        public string Ip1
        {
            get { return IP[0]; }

        }
        public StatesIpTram Ip1State = StatesIpTram.UNKNOWN; 
        public List<bool> Ip1StateList = new List<bool>();

        public List<long> Ip1PingTimeList = new List<long>();
        public long Ip1PingTime
        {
            get
            {
                return Ip1PingTimeList.Count > 0 ? (long)Ip1PingTimeList.Average():0;
            }
            set
            {
                Ip1PingTimeList.Add(value);
                RaisePropertyChanged("Ip1PingTime");
            }
        }
        public List<long> Ip2PingTimeList = new List<long>();
        public long Ip2PingTime
        {
            get
            {
                return Ip2PingTimeList.Count > 0 ? (long)Ip2PingTimeList.Average() : 0;
            }
            set
            {
                Ip2PingTimeList.Add(value);
                RaisePropertyChanged("Ip1PingTime");
            }
        }


        public string Ip2
        {
            get { return IP[1]; }

        }
        public StatesIpTram Ip2State = StatesIpTram.UNKNOWN;
        public List<bool> Ip2StateList = new List<bool>();

        private string _ip1Brush;
        public  string Ip1Brush
        {
            get { return _ip1Brush; }
            set
            {
                if (_ip1Brush != value)
                {
                    _ip1Brush = value;
                    RaisePropertyChanged("IP1Brush");
                }
            }
        }

        private string _ip2Brush;
        public string Ip2Brush
        {
            get { return _ip2Brush; }
            set
            {
                if (_ip2Brush != value)
                {
                    _ip2Brush = value;
                    RaisePropertyChanged("IP2Brush");
                }
            }
        }
    }
}

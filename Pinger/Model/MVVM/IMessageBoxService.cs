﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;


namespace Pinger.Model.MVVM
{
    public interface IMessageBoxService
    {
        bool ShowMessage(string text, string caption, MessageType messageType);
    }
}

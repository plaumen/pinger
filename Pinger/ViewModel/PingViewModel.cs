﻿using Pinger.Model.MVVM;
using Pinger.Model.PING;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Messaging;
using System.IO;

namespace Pinger.ViewModel
{
    public class PingViewModel : ObservableObject
    {
        public IIPAddressValidator IpValidator;
        public IPingSender Sender;
        private ITimerForCheckIpAddresses _timerForRefresh;
        public IMessageBoxFactory MessageFactory;
        public ITramsIPInfo TramsInfo;
        private IRunService _runner;
        public IConfigurationLoader Loader;

        public PingViewModel()
        {
            Trams = new ObservableCollection<SingleTram>();

            RunPuttyCommand = new Command(RunPutty);
            RunRDPCommand = new Command(RunRDP);
            RunWinSCPCommand = new Command(RunWinSCP);

            IpValidator = new IPAddressValidator();
            Sender = new PingSender(this);
            _timerForRefresh = new TimerForCheckIpAddresses(this);
            MessageFactory = new MessageBoxFactory();
            TramsInfo = new TramsIpInfo(this);
            _runner = new RunService();
            Loader = new ConfigurationLoader();
            TramName = "TramName";
            TramIP1 = "10.1.1.11";
            TramIP2 = "10.1.1.12";
            Timeout = 4000;
            RefreshPeriod = 60000;
        }

        public PingViewModel(IMessageBoxFactory factory, string tram1):this()
        {
            MessageFactory = factory;
            TramIP1 = tram1;
        }

        public PingViewModel(IPingSender sender, IConfigurationLoader loader) : this()
        {
            Sender = sender;
            Loader = loader;
        }


        private ObservableCollection<SingleTram> _trams;
        public ObservableCollection<SingleTram> Trams
        {
            get { return _trams; }
            set
            {
                if (_trams != value)
                {
                    _trams = value;
                    RaisePropertyChanged("Trams");
                }
            }
        }

        private string _tramName;
        public string TramName
        {
            get { return _tramName; }
            set
            {
                if (_tramName != value)
                {
                    _tramName = value;
                    RaisePropertyChanged("TramName");
                }
            }
        }

        private string _tramIP1;
        public string TramIP1
        {
            get { return _tramIP1; }
            set
            {
                if (_tramIP1 != value)
                {
                    _tramIP1 = value;
                    RaisePropertyChanged("TramIP1");
                }
            }
        }

        private string _tramIP2;
        public string TramIP2
        {
            get { return _tramIP2; }
            set
            {
                if (_tramIP2 != value)
                {
                    _tramIP2 = value;
                    RaisePropertyChanged("TramIP2");
                }
            }
        }

        private int _timeout;
        public int Timeout
        {
            get
            {
                return _timeout;
            }
            set
            {
                if(_timeout != value)
                {
                    _timeout = value;
                    RaisePropertyChanged("Timeout");
                }
            }
        }

        private int _refreshPeriod;
        public int RefreshPeriod
        {
            get
            {
                return _refreshPeriod;
            }
            set
            {
                if (_refreshPeriod != value)
                {
                    _refreshPeriod = value;
                    RaisePropertyChanged("RefreshPeriod");
                }
            }
        }

        private string _lastRefresh;
        public string LastRefresh
        {
            get { return _lastRefresh; }
            set
            {
                if (_lastRefresh != value)
                {
                    _lastRefresh = "Last ref.: " + value;
                    RaisePropertyChanged("LastRefresh");
                }
            }
        }

        public void RunPutty(Object tramFromUI)
        {
            _runner.RunPutty(tramFromUI);
        }

        public void RunRDP(Object tramFromUI)
        {
            _runner.RunRDP(tramFromUI);
        }

        public void RunWinSCP(Object tramFromUI)
        {
            _runner.RunWinSCP(tramFromUI);
        }

        public void AddTram()
        {
            if (!(IpValidator.CheckIp(TramIP1)&& IpValidator.CheckIp(TramIP2)))
            {
                IMessageBoxWrapper message = MessageFactory.CreateMessageBox();
                message.ShowMessage("Niepoprawny format ip !", "AddTramError", MessageType.Report);
            }
            else
            {
                Trams.Add(new SingleTram() { Name = TramName});
                Trams.Last().IP.Add(TramIP1);
                Trams.Last().IP.Add(TramIP2);
            }
        }

        public void Load()
        {
            ConfigurationReader reader = new ConfigurationReader();
            try
            {
                reader.LoadConfiguration();
            }
            catch (IOException loadException)
            {
                IMessageBoxWrapper wrapper = MessageFactory.CreateMessageBox();
                wrapper.ShowMessage("Problem z odczytem pliku Tram.csv." + loadException, "LoadError", MessageType.Report);
            }
            Trams.Clear();

            foreach (Tram tram in reader.TramListFromFile )
            {
                Trams.Add(new SingleTram() { Name = tram.Name });
                foreach (string ip in tram.IPAdresses )
                {
                    Trams.Last().IP.Add(ip);
                }
            }
        }

        public Command RunPuttyCommand { get; set; }
        public Command RunRDPCommand { get; set; }
        public Command RunWinSCPCommand { get; set; }

        public Command AddTramCommand
        {
            get
            {
                return  new Command(_=>AddTram());
            }
        }

        public Command RefreshCommand
        {
            get
            {      
                return new Command(_=> TramsInfo.CheckAllTrams());
            }
        }

        public Command LoadCommand
        {
            get
            {
                return new Command(_=>Load());
            }
        }

        public Command SaveCommand
        {
            get
            {
                return new Command(_ =>
                {
                    ConfigurationSaver saver = new ConfigurationSaver(Trams);
                    saver.SaveConfigurationsToFile();
                });
            }
        }

        public Command AutoRefreshCommand
        {
            get
            {
                return new Command(_=> _timerForRefresh.StartTimer());
            }
        }
    }
}
﻿using System;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pinger.ViewModel;
using Xunit;

namespace Pinger.Tests
{
    public class ColorChangerTests
    {
        [Fact]
        public void SetBrushFromRGB1_WhenCallWithArrayOfRGBGrayValues_ShouldSetAppropriateColor()
        {
            //Arrange
            int[] tab = {160, 160, 160};
            PingViewModel viewModel = new PingViewModel();
            viewModel.AddTram();
            ColorChanger changer = new ColorChanger(viewModel);
            //Act
            changer.SetBrushFromRGB1(tab,0);
            //Assert
            viewModel.Trams[0].Ip1Brush.Should().Be("#A0A0A0");
        }

        [Fact]
        public void SetBrushFromRGB2_WhenCallWithArrayOfRGBGrayValues_ShouldSetAppropriateColor()
        {
            //Arrange
            int[] tab = {160, 160, 160};
            PingViewModel viewModel = new PingViewModel();
            viewModel.AddTram();
            ColorChanger changer = new ColorChanger(viewModel);
            //Act
            changer.SetBrushFromRGB2(tab,0);
            //Assert
            viewModel.Trams[0].Ip2Brush.Should().Be("#A0A0A0");
        }

        [Fact]
        public void SetBrushFromRGB1_WhenCallWithArrayOfRGBGrenValues_ShouldSetAppropriateColor()
        {
            //Arrange
            int[] tab = {0, 255, 0};
            SingleTram tramTest = new SingleTram();
            PingViewModel viewModel = new PingViewModel();
            viewModel.AddTram();
            ColorChanger changer = new ColorChanger(viewModel);
            //Act
            changer.SetBrushFromRGB1(tab,0);
            //Assert
            viewModel.Trams[0].Ip1Brush.Should().Be("#0FF0");
        }

        [Fact]
        public void SetBrushFromRGB2_WhenCallWithArrayOfRGBGrenValues_ShouldSetAppropriateColor()
        {
            //Arrange
            int[] tab = {0, 255, 0};
            SingleTram tramTest = new SingleTram();
            PingViewModel viewModel = new PingViewModel();
            viewModel.AddTram();
            ColorChanger changer = new ColorChanger(viewModel);
            //Act
            changer.SetBrushFromRGB2(tab,0);
            //Assert
            viewModel.Trams[0].Ip2Brush.Should().Be("#0FF0");
        }

        [Fact]
        public void SetBrushFromRGB2_WhenCallWithArrayOfRGBRedValues_ShouldSetAppropriateColor()
        {
            //Arrange
            int[] tab = {255, 0, 0};
            SingleTram tramTest = new SingleTram();
            PingViewModel viewModel = new PingViewModel();
            viewModel.AddTram();
            ColorChanger changer = new ColorChanger(viewModel);
            //Act
            changer.SetBrushFromRGB2(tab,0);
            //Assert
            viewModel.Trams[0].Ip2Brush.Should().Be("#FF00");
        }

        [Fact]
        public void SetBrushFromRGB2_WhenCallWithTooLongArray_ShouldThrowException()
        {
            //Arrange
            int[] tab = {0, 0, 0, 0};
            SingleTram tramTest = new SingleTram();
            PingViewModel viewModel = new PingViewModel();
            viewModel.AddTram();
            ColorChanger changer = new ColorChanger(viewModel);

            //Act
            Action act = () => changer.SetBrushFromRGB2(tab,0);
            //Assert
            act.ShouldThrow<ArgumentException>();

        }

        [Fact]
        public void SetBrushFromRGB2_WhenCallWithIncorrectValueOfArray_ShouldThrowException()
        {
            //Arrange
            int[] tab = {300, 0, 10};
            SingleTram tramTest = new SingleTram();
            PingViewModel viewModel = new PingViewModel();
            viewModel.AddTram();
            ColorChanger changer = new ColorChanger(viewModel);

            //Act
            Action act = () => changer.SetBrushFromRGB2(tab,0);
            //Assert
            act.ShouldThrow<ArgumentException>();

        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pinger.Model.MVVM
{
    public class MessageBoxFactory : IMessageBoxFactory
    {
        public IMessageBoxService CreateMessageBox()
        {
            return new MessageBoxService();
        }
    }
}

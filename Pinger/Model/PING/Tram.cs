﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pinger.Model.PING
{
    public class Tram
    {
        public string Name { get; set; }
        public List<string> IPAdresses = new List<string>();
       
    }
}

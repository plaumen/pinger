﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Pinger.Views;
using Pinger.ViewModel;

namespace Pinger
{
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            PingView view = new PingView();
            PingViewModel model = new PingViewModel();
            model.Load();
            model.TramsInfo.SetButtonsIPInactive();
            model.TramsInfo.CheckAllTrams();
            view.DataContext = model;
            view.Show();
        }
    }
}

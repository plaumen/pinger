﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Pinger.Model.MVVM
{
    public class MessageBoxWrapper:IMessageBoxWrapper
    {
        public void ShowMessage(string text, string caption, MessageType messageType)
        {
            System.Windows.MessageBox.Show(text, caption);
        }
    }
}

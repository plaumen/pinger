﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pinger.Model.PING
{
    public static class ProcessRunner
    {
        public static void Run(string path, string args)
        {
            Process process = new Process();
            process.StartInfo.FileName = path;
            process.StartInfo.Arguments = args;
            process.Start();
        }
    }
}

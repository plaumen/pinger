﻿using System;

namespace Pinger.ViewModel
{
    public class ColorChanger:IColorChanger
    {
        private PingViewModel _viewModel;

        public ColorChanger(PingViewModel model)
        {
            _viewModel = model;
        }

        public void SetBrushFromRGB1(int[] RGBtab, int index)
        {
            if ((RGBtab.Length == 3)&&(!((RGBtab[0] < 0 || RGBtab[0] > 255) || (RGBtab[1] < 0 || RGBtab[1] > 255) || (RGBtab[2] < 0 || RGBtab[2] > 255))))
                _viewModel.Trams[index].Ip1Brush = "#" + RGBtab[0].ToString("X") + RGBtab[1].ToString("X") + RGBtab[2].ToString("X");
            else
                throw new ArgumentException("Zbyt duży rozmiar tablicy lub nieprawidlowe wartosci");
        }

        public void SetBrushFromRGB2(int[] RGBtab, int index)
        {
            if ((RGBtab.Length == 3) && (!((RGBtab[0] < 0 || RGBtab[0] > 255) || (RGBtab[1] < 0 || RGBtab[1] > 255) || (RGBtab[2] < 0 || RGBtab[2] > 255))))
                _viewModel.Trams[index].Ip2Brush = "#" + RGBtab[0].ToString("X") + RGBtab[1].ToString("X") + RGBtab[2].ToString("X");
            else            
                throw new ArgumentException("Zbyt duży rozmiar tablicy.");
            
        }
    }
}
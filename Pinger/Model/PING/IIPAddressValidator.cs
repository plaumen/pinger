﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pinger.Model.PING
{
    public interface IIPAddressValidator
    {
        bool CheckIp(string ip);
    }
}

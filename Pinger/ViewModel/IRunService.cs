﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pinger.ViewModel
{   
    public interface IRunService
    {
        void RunPutty(Object tramFromUI);
        void RunRDP(Object tramFromUI);
        void RunWinSCP(Object tramFromUI);
    }
}

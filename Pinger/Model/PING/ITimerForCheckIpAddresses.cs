﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Pinger.ViewModel;

namespace Pinger.Model.PING
{
    public interface ITimerForCheckIpAddresses
    {
        void StartTimer();
        void OnTimeEvent(object oSource, ElapsedEventArgs oElapsedEventArgs);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Pinger.Model.MVVM
{
    public class MessageBoxService:IMessageBoxService
    {
        public bool ShowMessage(string text, string caption, MessageType messageType)
        {
            System.Windows.MessageBox.Show(text, caption);
            return true;
        }
    }
}

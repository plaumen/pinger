﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Pinger.ViewModel;

namespace Pinger.Model.PING
{
    class ConfigurationSaver
    {
        public Collection<Tram> TramListToFile = new Collection<Tram>();

        public ConfigurationSaver(ObservableCollection<SingleTram> collectionTram )
        {
            for (int index = 0; index < collectionTram.Count; index++)
            {
                TramListToFile.Add(new Tram() {Name = collectionTram[index].Name});

                for (int i = 0; i <collectionTram[index].IP.Count; i++)
                {
                    TramListToFile[index].IPAdresses.Add(collectionTram[index].IP[i]);
                }
            }
        }

        public void SaveConfigurationsToFile()
        {
            var csv = new StringBuilder();
            foreach (Tram tram in TramListToFile)
            {
                var singleLine = String.Format("{0}", tram.Name);
                for (int i = 0; i < tram.IPAdresses.Count; i++)
                {
                    singleLine += String.Format(ConfigurationManager.AppSettings["SeparatorInCsv"]+"{0}", tram.IPAdresses[i]);
                }
                csv.AppendLine(singleLine);
            }
            File.WriteAllText("Trams.csv",csv.ToString());
        }
    }
}

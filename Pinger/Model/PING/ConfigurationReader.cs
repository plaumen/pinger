﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pinger.ViewModel;

namespace Pinger.Model.PING
{
    public class ConfigurationReader
    {
        public Collection<Tram> TramListFromFile = new Collection<Tram>();

        public void LoadConfiguration()
        {       
            var path = ConfigurationManager.AppSettings["FileWithTramPath"];
            string[] lines =    File.ReadAllLines(path).Where(arg => !string.IsNullOrWhiteSpace(arg)).ToArray();

                for(int index =0;index <lines.Length;index++)
                {
                    var afterTrim = lines[index].Trim();
                    var values = afterTrim.Split(Convert.ToChar(ConfigurationManager.AppSettings["SeparatorInCsv"]));
                        
                    TramListFromFile.Add(new Tram() {Name = values[0]});
                    for(int i =1; i<values.Length;i++) TramListFromFile.Last().IPAdresses.Add(values[i]);
                }
        }
    }
}

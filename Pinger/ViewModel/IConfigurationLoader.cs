﻿namespace Pinger.ViewModel
{
    public interface IConfigurationLoader
    {
        string GetConfiguration(string ConfigName);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Pinger.Model.PING
{
    public class IPAddressValidator:IIPAddressValidator
    {
        public bool CheckIp(string ip)
        {
            Regex validIp = new Regex("(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)");
            bool result= validIp.IsMatch(ip);
            return result;
        }
    }
}
